# Convert specified nodes to usefull format.
data "null_data_source" "nodes" {
  count = "${length(var.nodes)}"

  inputs = "${merge(
    map(
      "name", "node-${count.index}",
      "tags", "",
      "cpu", "1",
      "ram", "1024",
      "disk", "5",
      "type", "basic.${var.os_region}a",
      "image", "CentOS 7 Minimal 64-bit",
      "public_ip", "",
      "access_ip", ""
    ),
    var.defaults,
    var.nodes[count.index]
  )}"
}

# Gather information about system volume images.
data "openstack_images_image_v2" "system" {
  count = "${length(data.null_data_source.nodes.*.outputs)}"
  name  = "${data.null_data_source.nodes.*.outputs.image[count.index]}"
}

# Create key-pair for provision authentication.
resource "openstack_compute_keypair_v2" "provision" {
  name       = "terraform"
  region     = "${var.os_region}"
  public_key = "${file(var.keypair["public"])}"
}

# Create flavor for each node.
resource "openstack_compute_flavor_v2" "nodes" {
  count     = "${length(data.null_data_source.nodes.*.outputs)}"
  name      = "flavor-for-${data.null_data_source.nodes.*.outputs.name[count.index]}"
  is_public = false
  region    = "${var.os_region}"
  vcpus     = "${data.null_data_source.nodes.*.outputs.cpu[count.index]}"
  ram       = "${data.null_data_source.nodes.*.outputs.ram[count.index]}"
  disk      = 0
}

# Create nodes volumes.
resource "openstack_blockstorage_volume_v2" "system" {
  count       = "${length(data.null_data_source.nodes.*.outputs)}"
  name        = "disk-for-${data.null_data_source.nodes.*.outputs.name[count.index]}"
  region      = "${var.os_region}"
  size        = "${data.null_data_source.nodes.*.outputs.disk[count.index]}"
  volume_type = "${data.null_data_source.nodes.*.outputs.type[count.index]}"
  image_id    = "${data.openstack_images_image_v2.system.*.id[count.index]}"
}

# Create nodes.
resource "openstack_compute_instance_v2" "nodes" {
  count     = "${length(data.null_data_source.nodes.*.outputs)}"
  name      = "${data.null_data_source.nodes.*.outputs.name[count.index]}"
  metadata  = "${data.null_data_source.nodes.*.outputs[count.index]}"
  region    = "${var.os_region}"
  flavor_id = "${openstack_compute_flavor_v2.nodes.*.id[count.index]}"
  key_pair  = "${openstack_compute_keypair_v2.provision.name}"

  stop_before_destroy = true

  block_device {
    uuid             = "${openstack_blockstorage_volume_v2.system.*.id[count.index]}"
    source_type      = "volume"
    destination_type = "volume"
    boot_index       = 0
  }

  network {
    uuid        = "${openstack_networking_network_v2.default.id}"
    fixed_ip_v4 = "${data.null_data_source.nodes.*.outputs.access_ip[count.index]}"
  }
}
