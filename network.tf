# Gather external network information.
data "openstack_networking_network_v2" "external" {
  name = "${lookup(var.network, "external", "external-network")}"
}

# Create default router.
resource "openstack_networking_router_v2" "default" {
  name                = "default"
  region              = "${var.os_region}"
  admin_state_up      = "true"
  external_network_id = "${data.openstack_networking_network_v2.external.id}"
}

# Create default network.
resource "openstack_networking_network_v2" "default" {
  name           = "${lookup(var.network, "name", "default")}"
  region         = "${var.os_region}"
  admin_state_up = "true"
}

# Create default network subnet.
resource "openstack_networking_subnet_v2" "default" {
  region          = "${var.os_region}"
  network_id      = "${openstack_networking_network_v2.default.id}"
  cidr            = "${lookup(var.network, "cidr", "192.168.0.0/24")}"
  dns_nameservers = ["8.8.8.8", "8.8.4.4"]
}

# Create default route interface.
resource "openstack_networking_router_interface_v2" "default" {
  region    = "${var.os_region}"
  router_id = "${openstack_networking_router_v2.default.id}"
  subnet_id = "${openstack_networking_subnet_v2.default.id}"
}
