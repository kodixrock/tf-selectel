variable "os_auth_url" {
  type        = "string"
  description = "Openstack auth url"
  default     = "https://api.selvpc.ru/identity/v3"
}

variable "os_region" {
  type        = "string"
  description = "Openstack region name"
  default     = "ru-2"
}

variable "os_domain" {
  type        = "string"
  description = "Openstack domain name"
}

variable "os_tenant" {
  type        = "string"
  description = "Openstack tenant id"
}

variable "os_user" {
  type        = "string"
  description = "Openstack user name"
}

variable "os_secret" {
  type        = "string"
  description = "Openstack user password"
}

variable "keypair" {
  type        = "map"
  description = "Path to RSA key pair that will be used to authentication"

  default = {
    public  = "~/.ssh/id_rsa.pub"
    private = "~/.ssh/id_rsa"
  }
}

variable "network" {
  type        = "map"
  description = "Network configuration"

  default = {
    cidr     = "192.168.0.0/24"
    external = "external-network"
  }
}

variable "defaults" {
  type        = "map"
  description = "Node default configuration"

  default = {
    cpu  = 1
    ram  = 1024
    disk = 5
  }
}

variable "nodes" {
  type        = "list"
  description = "List of nodes to create"
  default     = []
}
