data "null_data_source" "result" {
  count                = "${length(data.null_data_source.nodes.*.outputs)}"
  has_computed_default = true

  inputs = "${merge(
      openstack_compute_instance_v2.nodes.*.metadata[count.index],
      map(
        "id", openstack_compute_instance_v2.nodes.*.id[count.index],
        "name", openstack_compute_instance_v2.nodes.*.name[count.index],
        "access_ip", openstack_compute_instance_v2.nodes.*.access_ip_v4[count.index]
      )
  )}"
}

output "public_ip" {
  description = "List of public IP"
  value       = "${local.public_ip}"
}

output "bastion_ip" {
  description = "Bastion IP address"
  value       = "${local.public_ip[0]}"
}

output "provision_key" {
  description = "Provision keys"
  value       = "${map("private", "${file(var.keypair["private"])}", "public", "${file(var.keypair["public"])}")}"
  sensitive   = true
}

output "nodes" {
  description = "List of created nodes"
  value       = "${data.null_data_source.result.*.outputs}"
}

locals {
  names = "${zipmap(openstack_compute_instance_v2.nodes.*.name, data.null_data_source.result.*.outputs)}"
}

output "names" {
  description = "List of created nodes grouped by name"
  value       = "${local.names}"
}
