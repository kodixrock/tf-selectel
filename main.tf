# Selectel provider configuration.
provider "openstack" {
  auth_url    = "${var.os_auth_url}"
  region      = "${var.os_region}"
  domain_name = "${var.os_domain}"
  tenant_id   = "${var.os_tenant}"
  user_name   = "${var.os_user}"
  password    = "${var.os_secret}"
}

locals {
  public_ip = "${distinct(compact(openstack_compute_instance_v2.nodes.*.metadata.public_ip))}"
}

#Create floating ip association.
resource "openstack_compute_floatingip_associate_v2" "nodes" {
  count       = "${length(local.public_ip)}"
  floating_ip = "${local.public_ip[count.index]}"
  instance_id = "${openstack_compute_instance_v2.nodes.*.id[index(openstack_compute_instance_v2.nodes.*.metadata.public_ip, local.public_ip[count.index])]}"
  region      = "${var.os_region}"
}
